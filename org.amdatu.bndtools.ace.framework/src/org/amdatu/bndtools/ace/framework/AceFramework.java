/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.ace.framework;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.amdatu.bndtools.ace.client.BndAceBundle;
import org.amdatu.bndtools.ace.client.BndAceClient;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.Version;
import org.osgi.framework.launch.Framework;

public class AceFramework implements Framework, ServiceListener {

	private static final String ACE_PROPERTIES_NAMESPACE = "org.amdatu.bndtools.ace";
	private static final String ACE_FEATURE_NAME_PROPERTY = ACE_PROPERTIES_NAMESPACE + ".feature";
	private static final String ACE_DISTRIBUTION_NAME_PROPERTY = ACE_PROPERTIES_NAMESPACE + ".distribution";
	private static final String ACE_TARGET_NAME_PROPERTY = ACE_PROPERTIES_NAMESPACE + ".target";

	private AceClientLauncher m_clientLauncher;

	private BndAceClient m_bndAceClient;

	public AceFramework(Map<String, String> configuration) {
		try {
			m_clientLauncher = new AceClientLauncher(configuration);
			m_bndAceClient = m_clientLauncher.getArtifactManager();

			String featureName = configuration.get(ACE_FEATURE_NAME_PROPERTY);
			if (featureName == null) {
				featureName = "bndtools-ace-feature";
			}

			String distributionName = configuration.get(ACE_DISTRIBUTION_NAME_PROPERTY);
			if (distributionName == null) {
				distributionName = "bndtools-ace-distribution";
			}
			
			String targetName = configuration.get(ACE_TARGET_NAME_PROPERTY);
			if (targetName == null) {
				targetName = "bndtools-ace-target";
			}

			m_bndAceClient.init(featureName, distributionName, targetName);
		} catch (Exception e) {
			System.err.println("Failed to initialize ACE client");
			throw new RuntimeException(e);
		}
	}

	@Override
	public FrameworkEvent waitForStop(long timeout) throws InterruptedException {
		return m_clientLauncher.waitForStop(timeout);
	}

	@Override
	public void stop() throws BundleException {
		m_clientLauncher.stop();
	}

	@Override
	public BundleContext getBundleContext() {
		return new BundleContext() {

			@Override
			public Bundle installBundle(String location) throws BundleException {
				try {
					location = location.replace("reference:", "");

					File file = new File(new URI(location));

					BndAceBundle bundle = m_bndAceClient.add(file);

					return bundle;
				} catch (Exception e) {
					throw new BundleException("Failed to install bundle ", e);
				}
			}

			@Override
			public boolean ungetService(ServiceReference<?> reference) {
				return false;
			}

			@Override
			public void removeServiceListener(ServiceListener listener) {

			}

			@Override
			public void removeFrameworkListener(FrameworkListener listener) {

			}

			@Override
			public void removeBundleListener(BundleListener listener) {

			}

			@Override
			public <S> ServiceRegistration<S> registerService(Class<S> clazz, S service,
					Dictionary<String, ?> properties) {
				return null;
			}

			@Override
			public ServiceRegistration<?> registerService(String clazz, Object service, Dictionary<String, ?> properties) {
				return null;
			}

			@Override
			public ServiceRegistration<?> registerService(String[] clazzes, Object service,
					Dictionary<String, ?> properties) {
				return null;
			}

			@Override
			public Bundle installBundle(String location, InputStream input) throws BundleException {
				return null;
			}

			@Override
			public <S> Collection<ServiceReference<S>> getServiceReferences(Class<S> clazz, String filter)
					throws InvalidSyntaxException {
				return null;
			}

			@Override
			public ServiceReference<?>[] getServiceReferences(String clazz, String filter)
					throws InvalidSyntaxException {
				return null;
			}

			@Override
			public <S> ServiceReference<S> getServiceReference(Class<S> clazz) {
				return null;
			}

			@Override
			public ServiceReference<?> getServiceReference(String clazz) {
				return null;
			}

			@Override
			public <S> S getService(ServiceReference<S> reference) {
				return null;
			}

			@Override
			public String getProperty(String key) {
				return null;
			}

			@Override
			public File getDataFile(String filename) {
				return null;
			}

			@Override
			public Bundle[] getBundles() {
				return new Bundle[0];
			}

			@Override
			public Bundle getBundle(String location) {
				return null;
			}

			@Override
			public Bundle getBundle(long id) {
				return null;
			}

			@Override
			public Bundle getBundle() {
				return null;
			}

			@Override
			public ServiceReference<?>[] getAllServiceReferences(String clazz, String filter)
					throws InvalidSyntaxException {
				return null;
			}

			@Override
			public Filter createFilter(String filter) throws InvalidSyntaxException {
				return null;
			}

			@Override
			public void addServiceListener(ServiceListener listener, String filter) throws InvalidSyntaxException {
			}

			@Override
			public void addServiceListener(ServiceListener listener) {
			}

			@Override
			public void addFrameworkListener(FrameworkListener listener) {
			}

			@Override
			public void addBundleListener(BundleListener listener) {
			}
		};
	}

	@Override
	public int getState() {
		return 0;
	}

	@Override
	public Dictionary<String, String> getHeaders() {
		return null;
	}

	@Override
	public ServiceReference<?>[] getRegisteredServices() {
		return null;
	}

	@Override
	public ServiceReference<?>[] getServicesInUse() {
		return null;
	}

	@Override
	public boolean hasPermission(Object permission) {
		return false;
	}

	@Override
	public URL getResource(String name) {
		return null;
	}

	@Override
	public Dictionary<String, String> getHeaders(String locale) {
		return null;
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		return null;
	}

	@Override
	public Enumeration<URL> getResources(String name) throws IOException {
		return null;
	}

	@Override
	public long getLastModified() {
		return 0;
	}

	@Override
	public void stop(int options) throws BundleException {

	}

	@Override
	public Map<X509Certificate, List<X509Certificate>> getSignerCertificates(int signersType) {
		return null;
	}

	@Override
	public Version getVersion() {
		return null;
	}

	@Override
	public File getDataFile(String filename) {
		return null;
	}

	@Override
	public int compareTo(Bundle o) {
		return 0;
	}

	@Override
	public void init() throws BundleException {
	}

	@Override
	public void start() throws BundleException {
	}

	@Override
	public void start(int options) throws BundleException {
	}

	@Override
	public void uninstall() throws BundleException {
	}

	@Override
	public void update() throws BundleException {
	}

	@Override
	public void update(InputStream in) throws BundleException {
	}

	@Override
	public long getBundleId() {
		return 0;
	}

	@Override
	public String getLocation() {
		return null;
	}

	@Override
	public String getSymbolicName() {
		return null;
	}

	@Override
	public Enumeration<String> getEntryPaths(String path) {
		return null;
	}

	@Override
	public URL getEntry(String path) {
		return null;
	}

	@Override
	public Enumeration<URL> findEntries(String path, String filePattern, boolean recurse) {
		return null;
	}

	@Override
	public <A> A adapt(Class<A> type) {
		return null;
	}

	@Override
	public void serviceChanged(ServiceEvent event) {
	}

}
